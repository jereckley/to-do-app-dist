export class ToDoListItem {
    toggled() {
        this.toggleItemAtIndex.emit(this.itemIndex);
    }
    remove() {
        this.removeItemAtIndex.emit(this.itemIndex);
    }
    render() {
        return (h("div", { class: 'list-item-containing-div' },
            h("to-do-checkbox", { isChecked: this.toDoItem.done }),
            h("div", { class: 'item-text' }, this.toDoItem.description),
            h("to-do-button", { isRed: true, onClick: this.remove.bind(this) }, "delete")));
    }
    static get is() { return "to-do-list-item"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "itemIndex": {
            "type": Number,
            "attr": "item-index"
        },
        "toDoItem": {
            "type": "Any",
            "attr": "to-do-item"
        }
    }; }
    static get events() { return [{
            "name": "removeItemAtIndex",
            "method": "removeItemAtIndex",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "toggleItemAtIndex",
            "method": "toggleItemAtIndex",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "checkboxToggled",
            "method": "toggled"
        }]; }
    static get style() { return "/**style-placeholder:to-do-list-item:**/"; }
}
