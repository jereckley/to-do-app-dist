export class ToDoCheckbox {
    checkboxChanged() {
        this.checkboxToggled.emit();
    }
    render() {
        return (h("div", { class: 'checkbox-div' },
            h("input", { type: 'checkbox', onChange: this.checkboxChanged.bind(this), checked: this.isChecked })));
    }
    static get is() { return "to-do-checkbox"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "isChecked": {
            "type": Boolean,
            "attr": "is-checked"
        }
    }; }
    static get events() { return [{
            "name": "checkboxToggled",
            "method": "checkboxToggled",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "/**style-placeholder:to-do-checkbox:**/"; }
}
