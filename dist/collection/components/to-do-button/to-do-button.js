export class ToDoButton {
    render() {
        return (h("div", { class: 'button-container ' + (this.isRed ? 'button-red' : '') },
            h("div", { class: 'button-text' },
                h("slot", null))));
    }
    static get is() { return "to-do-button"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "isRed": {
            "type": "Any",
            "attr": "is-red"
        }
    }; }
    static get style() { return "/**style-placeholder:to-do-button:**/"; }
}
