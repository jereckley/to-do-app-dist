export class ToDoInput {
    textChange(event) {
        this.toDoInputTextUpdated.emit(event.key);
    }
    render() {
        return (h("div", { class: 'input-div' },
            h("input", { type: 'text', onKeyDown: this.textChange.bind(this) })));
    }
    static get is() { return "to-do-input"; }
    static get encapsulation() { return "shadow"; }
    static get events() { return [{
            "name": "toDoInputTextUpdated",
            "method": "toDoInputTextUpdated",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "/**style-placeholder:to-do-input:**/"; }
}
