export class ToDo {
    constructor() {
        this.list = [
            {
                done: false,
                description: "mow the lawn"
            },
            {
                done: false,
                description: "feed the dog"
            },
            {
                done: true,
                description: "eat food"
            }
        ];
    }
    addToDoEventHandler(event) {
        this.list = [...this.list, { done: false, description: event.detail }];
    }
    removeItemAtIndexHandler(event) {
        this.list.splice(event.detail, 1);
        this.list = [...this.list];
    }
    toggleItemAtIndexHandler(event) {
        console.log(event.detail);
        this.list[event.detail].done = !this.list[event.detail].done;
        this.list.map(item => { return Object.assign({}, item); });
        this.list = [...this.list];
    }
    render() {
        return (h("div", { class: 'happy' },
            h("to-do-header", null),
            h("to-do-body", { listAwesome: this.list })));
    }
    static get is() { return "to-do"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "list": {
            "state": true
        }
    }; }
    static get listeners() { return [{
            "name": "addToDoEvent",
            "method": "addToDoEventHandler"
        }, {
            "name": "removeItemAtIndex",
            "method": "removeItemAtIndexHandler"
        }, {
            "name": "toggleItemAtIndex",
            "method": "toggleItemAtIndexHandler"
        }]; }
    static get style() { return "/**style-placeholder:to-do:**/"; }
}
