export class ToDoHeader {
    render() {
        return (h("div", null,
            h("to-do-new", null)));
    }
    static get is() { return "to-do-header"; }
    static get encapsulation() { return "shadow"; }
    static get style() { return "/**style-placeholder:to-do-header:**/"; }
}
