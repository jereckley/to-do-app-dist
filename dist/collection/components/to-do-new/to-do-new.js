export class ToDoNew {
    constructor() {
        this.toDoText = '';
    }
    toDoInputTextUpdatedHandler(event) {
        if (event.detail.toLowerCase() === 'backspace') {
            if (this.toDoText.length > 0)
                this.toDoText = this.toDoText.slice(0, -1);
        }
        else if (event.detail.toLowerCase() === 'enter') {
            if (this.toDoText.length > 0)
                this.addToDoEvent.emit(this.toDoText);
        }
        else {
            this.toDoText += event.detail;
        }
    }
    addToDo() {
        if (this.toDoText.length > 0)
            this.addToDoEvent.emit(this.toDoText);
    }
    render() {
        return (h("div", { class: 'new-div' },
            h("to-do-input", null),
            h("to-do-button", { onClick: this.addToDo.bind(this) }, "add to do")));
    }
    static get is() { return "to-do-new"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "toDoText": {
            "state": true
        }
    }; }
    static get events() { return [{
            "name": "addToDoEvent",
            "method": "addToDoEvent",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "toDoInputTextUpdated",
            "method": "toDoInputTextUpdatedHandler"
        }]; }
    static get style() { return "/**style-placeholder:to-do-new:**/"; }
}
