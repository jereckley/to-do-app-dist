export class ToDoLists {
    render() {
        return (h("div", { class: 'list-containing-div' },
            h("div", { class: 'title-div' },
                this.listTitle,
                ":"),
            h("div", { class: 'list-div' },
                h("slot", null))));
    }
    static get is() { return "to-do-lists"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "listTitle": {
            "type": "Any",
            "attr": "list-title"
        }
    }; }
    static get style() { return "/**style-placeholder:to-do-lists:**/"; }
}
