export class ToDoBody {
    render() {
        return (h("div", null,
            h("to-do-lists", { "list-title": 'To Do' }, this.listAwesome ? this.listAwesome.map((todo, index) => {
                return todo.done ? null : h("to-do-list-item", { toDoItem: todo, itemIndex: index });
            }) : null),
            h("to-do-lists", { "list-title": 'Done' }, this.listAwesome ? this.listAwesome.map((todo, index) => {
                return todo.done ? h("to-do-list-item", { toDoItem: todo, itemIndex: index }) : null;
            }) : null)));
    }
    static get is() { return "to-do-body"; }
    static get encapsulation() { return "shadow"; }
    static get properties() { return {
        "listAwesome": {
            "type": "Any",
            "attr": "list-awesome"
        }
    }; }
    static get style() { return "/**style-placeholder:to-do-body:**/"; }
}
