import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
export declare class ToDoCheckbox {
    isChecked: boolean;
    checkboxToggled: EventEmitter;
    checkboxChanged(): void;
    render(): JSX.Element;
}
