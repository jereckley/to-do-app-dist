import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
import { ToDos } from "../to-do-body/to-do-body";
export declare class ToDoListItem {
    toDoItem: ToDos;
    itemIndex: number;
    removeItemAtIndex: EventEmitter;
    toggleItemAtIndex: EventEmitter;
    toggled(): void;
    remove(): void;
    render(): JSX.Element;
}
