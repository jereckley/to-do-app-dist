import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
export declare class ToDoNew {
    toDoText: string;
    addToDoEvent: EventEmitter;
    toDoInputTextUpdatedHandler(event: any): void;
    addToDo(): void;
    render(): JSX.Element;
}
