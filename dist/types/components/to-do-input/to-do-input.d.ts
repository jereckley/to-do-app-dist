import '../../stencil.core';
import { EventEmitter } from '../../stencil.core';
export declare class ToDoInput {
    toDoInputTextUpdated: EventEmitter;
    textChange(event: any): void;
    render(): JSX.Element;
}
