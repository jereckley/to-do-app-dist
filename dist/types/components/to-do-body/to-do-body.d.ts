import '../../stencil.core';
export interface ToDos {
    done: boolean;
    description: string;
}
export declare class ToDoBody {
    listAwesome: Array<ToDos>;
    render(): JSX.Element;
}
