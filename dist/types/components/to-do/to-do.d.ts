import '../../stencil.core';
import { ToDos } from "../to-do-body/to-do-body";
export declare class ToDo {
    list: Array<ToDos>;
    addToDoEventHandler(event: CustomEvent): void;
    removeItemAtIndexHandler(event: CustomEvent): void;
    toggleItemAtIndexHandler(event: CustomEvent): void;
    render(): JSX.Element;
}
