// ToDo: Custom Elements Define Library, ES Module/ES5 Target
import { defineCustomElement } from './to-do.core.js';
import {
  ToDo,
  ToDoBody,
  ToDoButton,
  ToDoCheckbox,
  ToDoHeader,
  ToDoInput,
  ToDoListItem,
  ToDoLists,
  ToDoNew
} from './to-do.components.js';

export function defineCustomElements(window, opts) {
  defineCustomElement(window, [
    ToDo,
    ToDoBody,
    ToDoButton,
    ToDoCheckbox,
    ToDoHeader,
    ToDoInput,
    ToDoListItem,
    ToDoLists,
    ToDoNew
  ], opts);
}