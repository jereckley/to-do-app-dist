// ToDo: Host Data, ES Module/ES5 Target

export var ToDo = ["to-do",function(o){return(o.scoped?import("./gae4cshn.sc.js"):import("./gae4cshn.js")).then(function(m){return m.ToDo})},1,[["list",5]],1,[["addToDoEvent","addToDoEventHandler"],["removeItemAtIndex","removeItemAtIndexHandler"],["toggleItemAtIndex","toggleItemAtIndexHandler"]]];

export var ToDoBody = ["to-do-body",function(o){return(o.scoped?import("./iougl3qr.sc.js"):import("./iougl3qr.js")).then(function(m){return m.ToDoBody})},1,[["listAwesome",1]],1];

export var ToDoButton = ["to-do-button",function(o){return(o.scoped?import("./r3vwd2jl.sc.js"):import("./r3vwd2jl.js")).then(function(m){return m.ToDoButton})},1,[["isRed",1,0,"is-red",1]],1];

export var ToDoCheckbox = ["to-do-checkbox",function(o){return(o.scoped?import("./iougl3qr.sc.js"):import("./iougl3qr.js")).then(function(m){return m.ToDoCheckbox})},1,[["isChecked",1,0,"is-checked",3]],1];

export var ToDoHeader = ["to-do-header",function(o){return(o.scoped?import("./gae4cshn.sc.js"):import("./gae4cshn.js")).then(function(m){return m.ToDoHeader})},1,0,1];

export var ToDoInput = ["to-do-input",function(o){return(o.scoped?import("./gae4cshn.sc.js"):import("./gae4cshn.js")).then(function(m){return m.ToDoInput})},1,0,1];

export var ToDoListItem = ["to-do-list-item",function(o){return(o.scoped?import("./iougl3qr.sc.js"):import("./iougl3qr.js")).then(function(m){return m.ToDoListItem})},1,[["itemIndex",1,0,"item-index",4],["toDoItem",1]],1,[["checkboxToggled","toggled"]]];

export var ToDoLists = ["to-do-lists",function(o){return(o.scoped?import("./iougl3qr.sc.js"):import("./iougl3qr.js")).then(function(m){return m.ToDoLists})},1,[["listTitle",1,0,"list-title",1]],1];

export var ToDoNew = ["to-do-new",function(o){return(o.scoped?import("./gae4cshn.sc.js"):import("./gae4cshn.js")).then(function(m){return m.ToDoNew})},1,[["toDoText",5]],1,[["toDoInputTextUpdated","toDoInputTextUpdatedHandler"]]];